# Platform Dashboard

This project provides the codebase for the Platform Dashboard. The code is split into two segments:

- `/client` - the React frontend
- `/backend` - a small Node app that proxies requests to the configured Kube API server

## Running it Locally

Assuming you have a Kubeconfig file already configured (feel free to go to [platform.it.vt.edu](https://platform.it.vt.edu) and go to the Kubeconfig setup link), you can use the following command to start a container locally:

```shell
docker run -dp 3000:3000 -e CLIENT_MODE=LOCAL_KUBECONFIG -v $HOME/.kube:/root/.kube code.vt.edu:5005/it-common-platform/support/dashboard:latest
```

Once the container is running, you can open the application at [http://localhost:3000](http://locahost:3000). 

Note that if you have multiple contexts in your Kubeconfig, the app will use the current one. You can even change the context and refresh the page without needing to restart the app!


## Development

To contribute to the project, you will need to have a configured Kubeconfig file found at `~/.kube/config`. Once you have that, you can spin up the development environment by simply running:

```shell
docker compose up
```

Once the containers are running, you can access the app at [https://dashboard.localhost.devcom.vt.edu/](https://dashboard.localhost.devcom.vt.edu/).

## Deployment

Git pushes to origin will always build an image. That image will be deployed under the following conditions:

* [aws-dvlp](https://dashboard.dvlp.aws.itcp.cloud.vt.edu/) when the branch **is not** `${CI_DEFAULT_BRANCH}` (e.g. `main`)
* [op-dvlp](https://dashboard.dvlp.op.itcp.cloud.vt.edu/) when the branch **is not** `${CI_DEFAULT_BRANCH}` (e.g. `main`)
* [op-pprd](https://dashboard.pprd.op.itcp.cloud.vt.edu/) when the branch **is** `${CI_DEFAULT_BRANCH}` (e.g. `main`) 
* [aws-prod](https://dashboard.platform.it.vt.edu/) when a commit hash has the **prod** tag applied

## Configuration

The backend can currently run in two modes, which determines whether the backend uses a Kubeconfig file for the configuration or requires a Bearer token to be applied through the API (which is then validated and passed through). This makes it easy to test locally while supporting a multi-tenant/unprivileged deployment in production.

### Kubeconfig Mode

To run in this mode, the following environment variables can be specified:

- `CLIENT_MODE` - set to `LOCAL_KUBECONFIG`
- `KUBE_CONFIG_FILE_LOCATION` (defaults to `/root/.kube/config`) - the filepath of the Kubeconfig file to use

### Token Passthrough

In this mode, the API endpoints are configured to require the auth token to be provided. Eventually, this will use OIDC discovery and the app will perform the auth flow. But, until then, it simply requests a token from the token configurator. To use this mode, the following environment variables must be provided.

- `CLIENT_MODE` - set to `AUTH_HEADER`
- `JWKS_ENDPOINT` - set to the location of the JWKS endpoint (e.g., https://login.prod.clusters.aws.platform.it.cloud.vt.edu/keys)
- `JWT_AUDIENCE` - the audience that must exist in the JWT token
- `JWT_ISSUER` - the issuer on the JWT token
- `TOKEN_APP_URL` - the location to send users to get a token
- `K8S_API_ENDPOINT` - the API endpoint for the Kube API
