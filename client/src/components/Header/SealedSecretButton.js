import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";

export const SealedSecretButton = ({ activeNamespace }) => {
  return (
    <Nav.Link className="pe-3" to={`/namespaces/${activeNamespace}/sealed-secrets`} as={Link}>
      Sealed Secrets Tool
    </Nav.Link>
  );
}