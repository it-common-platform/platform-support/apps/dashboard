import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import { LoadingSpinner } from "../LoadingSpinner";

export const DeploymentCard = ({ deployments }) => {
  return (
    <Card>
      <Card.Header>Deployments</Card.Header>
      <Card.Body>
        { deployments ? 
          <DeploymentTable deployments={deployments} />
          : <LoadingSpinner text="Loading deployments..." />
        }
      </Card.Body>
    </Card>
  )
};

const DeploymentTable = ({ deployments }) => {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Name</th>
          <th>Image</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        { deployments.map(deployment => (
          <tr key={deployment.metadata.name}>
            <td>{ deployment.metadata.name }</td>
            <td>{ deployment.spec.template.spec.containers[0].image }</td>
            <td>-</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}