import { useContext } from "react";
import TableContext from "../context/TableContext";

export const useFilter = (filterName) => {
  const { filter } = useContext(TableContext);

  if (filter && filter[filterName]) return filter[filterName];

  console.warn(`No filter with the name: ${filterName} is defined`);
}