const { findIssues } = require("../../src/issuesFinder");
let k8sData = require("./samples/good.json");

describe("setup", () => {
  it("skips when pods are missing", () => {
    expect(findIssues({ ...k8sData, pods: undefined }).length).toBe(0);
  });

  it("skips when kustomizations are missing", () => {
    expect(findIssues({ ...k8sData, kustomizations: undefined }).length).toBe(0);
  });

  it("skips when ingresses are missing", () => {
    expect(findIssues({ ...k8sData, ingresses: undefined }).length).toBe(0);
  });

  it("skips when services are missing", () => {
  expect(findIssues({ ...k8sData, services: undefined }).length).toBe(0);
  });

  it("skips when certificates are missing", () => {
    expect(findIssues({ ...k8sData, certificates: undefined }).length).toBe(0);
  });
});

it("finds no issues with the base set", () => {
  const issues = findIssues(k8sData);
  expect(issues.length).toBe(0);
});

describe("Kustomization issues", () => {
  it("creates an issue when the Kustomization is marked as failed", () => {
    const failedKustomization = require("./samples/kustomization-failed-apply.json");

    const issues = findIssues({ ...k8sData, kustomizations: [ failedKustomization ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("Unable to apply manifests");
    expect(issues[0].description).toContain(failedKustomization.status.conditions[0].message);
  });
});

describe("Ingress issues", () => {
  it("creates an issue when using the non-stable apiVersion", () => {
    const oldIngress = require("./samples/ingress-old-apiVersion.json");

    const issues = findIssues({ ...k8sData, ingresses: [ oldIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("needs to be updated to the networking.k8s.io apiVersion");
  });

  it("creates an issue when the backend service isn't found", () => {
    const disconnectedIngress = require("./samples/ingress-unrecognized-service-name.json");

    const issues = findIssues({ ...k8sData, ingresses: [ disconnectedIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("references a Service named unrecognized, but no matching Service is found");
  });

  it("creates an issue when the backend service port number isn't found on the Service", () => {
    const disconnectedIngress = require("./samples/ingress-unrecognized-service-port-number.json");

    const issues = findIssues({ ...k8sData, ingresses: [ disconnectedIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("references port 5000 on Service sample, but that port isn't found on the Service");
  });

  it("creates an issue when the backend service port name isn't found on the Service", () => {
    const disconnectedIngress = require("./samples/ingress-unrecognized-service-port-name.json");

    const issues = findIssues({ ...k8sData, ingresses: [ disconnectedIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("references a port named unrecognized on Service sample, but that port isn't found on the Service");
  });

  it("creates an issue when no TLS information is provided", () => {
    const insecureIngress = require("./samples/ingress-no-tls-configured.json");

    const issues = findIssues({ ...k8sData, ingresses: [ insecureIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("Ingress sample doesn't specify any TLS configuration");
  });

  it("creates an issue when the TLS hosts don't match any Certificate objects", () => {
    const misconfiguredIngress = require("./samples/ingress-mismatched-tls-hostnames.json");

    const issues = findIssues({ ...k8sData, ingresses: [ misconfiguredIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("Ingress sample doesn't have a Certificate that matches all defined TLS hosts");
  });

  it("creates an issue when there is a mismatch between the Ingress and Certificate secret", () => {
    const misconfiguredIngress = require("./samples/ingress-mismatched-tls-secret-name.json");

    const issues = findIssues({ ...k8sData, ingresses: [ misconfiguredIngress ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("Ingress sample indicates the TLS cert should be stored at wrong-secret, but all Certificates are using a different secret name");
  });
});

describe("Service issues", () => {
  it("creates an issue when no pods have a matching label", () => {
    const misconfiguredService = require("./samples/service-mismatched-labels.json");

    const issues = findIssues({ ...k8sData, services: [ misconfiguredService ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("Service sample has a selector that does not currently match any running pods");
  });
});

describe("Certificate issues", () => {
  it("creates an issue when the commonName is not listed as a dnsName", () => {
    const misconfiguredCert = require("./samples/certificate-cn-not-in-dn.json");

    const issues = findIssues({ ...k8sData, certificates: [ misconfiguredCert ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("error");
    expect(issues[0].description).toContain("The Certificate sample contains a commonName (unknown.platform.it.vt.edu) that isn't listed in the dnsNames");
  });
});

describe("Pod issues", () => {
  it("creates a warning when a pod container doesn't have memory limits defined", () => {
    const misconfiguredPod = require("./samples/pod-no-memory-limits-defined.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify memory limits");
  });

  it("creates both limit warnings if no resource limits are defined", () => {
    const misconfiguredPod = require("./samples/pod-no-limits-defined.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify memory limits");
  });

  it("creates a warning when a pod container doesn't have CPU requests defined", () => {
    const misconfiguredPod = require("./samples/pod-no-cpu-requests-defined.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify CPU requests");
  });

  it("creates a warning when a pod container doesn't have memory requests defined", () => {
    const misconfiguredPod = require("./samples/pod-no-memory-requests-defined.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify memory requests");
  });

  it("creates both requests warnings if no resource requests are defined", () => {
    const misconfiguredPod = require("./samples/pod-no-requests-defined.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(2);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify CPU requests");
    expect(issues[1].type).toBe("warning");
    expect(issues[1].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify memory requests");
  });

  it("creates a warning if no livenessProbe is defined", () => {
    const misconfiguredPod = require("./samples/pod-no-liveness-probe.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("warning");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify a liveness probe");
  });

  it("creates an info issue if no readinessProbe is defined", () => {
    const misconfiguredPod = require("./samples/pod-no-readiness-probe.json");

    const issues = findIssues({ ...k8sData, pods: [ misconfiguredPod ]});
    expect(issues.length).toBe(1);
    expect(issues[0].type).toBe("info");
    expect(issues[0].description).toContain("Container app on pod sample-8476f4fbcb-7n2x7 does not specify a readiness probe");
  });
});