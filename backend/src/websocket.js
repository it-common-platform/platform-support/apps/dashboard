const k8sClient = require("./k8s/client");
const k8sService = require("./k8s/k8sService");
const jwt = require("jsonwebtoken");
const zlib = require("zlib");

const clientWatchers = {};

const webSocketHandler = (ws, req) => {
  ws.on("message", (msg) => {
    try {
      const payload = JSON.parse(msg);
      let identifier = `local-${req.param}`;

      if (payload.type === "heartbeat") {
        return;
      }

      const token = (req.user) ? req.user.accessToken : payload.token;
      if (token) {
        const decodedToken = jwt.decode(token);
        identifier = decodedToken.sub;
      }

      clientWatchers[identifier] = [];
      if (req.params.namespace) {
        fetchCurrentStateForNamespace(req.params.namespace, identifier, token, ws)
          .then(() => listenForNamespaceEvents(req.params.namespace, identifier, token, ws));
      }
      else {
        fetchCurrentGlobalState(identifier, token, ws)
          .then(() => listenForGlobalEvents(identifier, token, ws));
      }

      ws.on("close", () => {
        clientWatchers[identifier].forEach(() => {
          try { req.abort(); } catch {}
        });
      });
    } catch (err) {
      console.error(err);
    }
  });
};

const fetchCurrentStateForNamespace = async (namespace, identifier, authToken, ws) => {
  return Promise.all([
    k8sService.getCertificates(identifier, authToken, namespace),
    k8sService.getDeployments(identifier, authToken, namespace),
    k8sService.getEndpoints(identifier, authToken, namespace),
    k8sService.getIngresses(identifier, authToken, namespace),
    k8sService.getKustomizations(identifier, authToken, namespace),
    k8sService.getPods(identifier, authToken, namespace),
    k8sService.getServices(identifier, authToken, namespace),
  ]).then(([ certificates, deployments, endpoints, ingresses, kustomizations, pods, services]) => {
    ws.send(convertData({
      type: "event",
      event : {
        type: "SET",
        data: {
          Certificate: certificates, 
          Deployment: deployments, 
          Endpoints: endpoints, 
          Ingress: ingresses, 
          Kustomization: kustomizations, 
          Pod: pods, 
          Service: services,
        }
      },
    }));
  })
  .catch((err) => console.error(err));
};

const listenForNamespaceEvents = (namespace, identifier, authToken, ws) => {
  listenForSpecificResources("v1", "pods", namespace, identifier, authToken, ws);
  listenForSpecificResources("apps/v1", "deployments", namespace, identifier, authToken, ws);
  listenForSpecificResources("v1", "services", namespace, identifier, authToken, ws);
  listenForSpecificResources("v1", "endpoints", namespace, identifier, authToken, ws);
  listenForSpecificResources("cert-manager.io/v1", "certificates", namespace, identifier, authToken, ws);
  listenForSpecificResources("networking.k8s.io/v1", "ingresses", namespace, identifier, authToken, ws);
  listenForSpecificResources("kustomize.toolkit.fluxcd.io/v1beta2", "kustomizations", namespace, identifier, authToken, ws);
  listenForSpecificResources("helm.toolkit.fluxcd.io/v2beta1", "helmreleases", namespace, identifier, authToken, ws);
};

const fetchCurrentGlobalState = async (identifier, authToken, ws) => {
  return Promise.all([
    k8sService.getNodes(identifier, authToken),
    k8sService.getPods(identifier, authToken),
  ]).then(([ nodes, pods, ]) => {
    ws.send(convertData({
      type: "event",
      event : {
        type: "SET",
        data: {
          Pod: pods,
          Node: nodes,
        }
      },
    }));
  })
  .catch((err) => console.error(err));
};

const listenForGlobalEvents = (identifier, authToken, ws) => {
  listenForSpecificResources("v1", "pods", undefined, identifier, authToken, ws);
  listenForSpecificResources("v1", "nodes", undefined, identifier, authToken, ws);
};

const listenForSpecificResources = (apiVersion, kind, namespace, identifier, authToken, ws) => {
  const watcher = k8sClient.getWatcher(identifier, authToken);
  const apiPrefix = (apiVersion.indexOf("/") > -1) ? "apis" : "api";
  const url = `/${apiPrefix}/${apiVersion}/${namespace ? `namespaces/${namespace}/` : ""}${kind}`;

  watcher.watch(url, {}, (type, apiObj, watchObj) => {
    ws.send(convertData({ type: "event", event: { type, object: apiObj }}));
  }, () => {
    ws.send(convertData({ type: "disconnect", resource: "pods" }));
    ws.close();
  }).then(req => {
    if (!clientWatchers[identifier])
      clientWatchers[identifier] = [];
    clientWatchers[identifier].push(req);
  }).catch(err => {
    console.error(err);
  });
};

const convertData = (obj) => {
  return zlib.gzipSync(JSON.stringify(obj)).toString("base64");
};

module.exports = webSocketHandler;
