const session = require("express-session");
const redis = require("redis");
const RedisStore = require("connect-redis").default
const config = require("./config");

function addSessionHandling(app) {
  const redisClient = redis.createClient({
    url: "redis://" + config.getRedisHost() + ":6379"
  });
  redisClient.connect();
  redisClient.on("error", (err) => console.error("Redis error", err));
  redisClient.on("connect", () => console.log("Connected to redis successfully"));
  
  app.set("trust proxy", 1);
  app.use(session({
    store: new RedisStore({ client: redisClient }),
    secret: "session super secret",
    cookie: {
      secure: true,
    }
  }));  
}

module.exports = {
  addSessionHandling,
};