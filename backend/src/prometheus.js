const promBundle = require("express-prom-bundle");

function addMetrics(app) {
  const metricsMiddleware = promBundle({ 
    includeStatusCode: true,
    includeMethod: true,
    includePath: true,
    promClient: {
      collectDefaultMetrics: {}
    }
  });
  
  app.use(metricsMiddleware);  
}


module.exports = {
  addMetrics,
};