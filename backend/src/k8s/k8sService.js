const k8sClient = require("./client");
const stream = require("stream");

const getDeployments = async (identifier, token, namespace) => {
  const client = k8sClient.getAppsApiClient(identifier, token);
  const deploymentRequest = await client.listNamespacedDeployment(namespace);
  deploymentRequest.body.items.forEach(i => i.apiVersion = deploymentRequest.body.apiVersion);
  return deploymentRequest.body.items;
};

const getPods = async (identifier, token, namespace) => {
  const client = k8sClient.getCoreApiClient(identifier, token);
  const podRequest = await (namespace ? client.listNamespacedPod(namespace) : client.listPodForAllNamespaces());
  podRequest.body.items.forEach(i => i.apiVersion = podRequest.body.apiVersion);
  return podRequest.body.items;
};

const getNodes = async (identifier, token, namespace) => {
  const client = k8sClient.getCoreApiClient(identifier, token);
  const nodeRequest = await client.listNode(namespace);
  nodeRequest.body.items.forEach(i => i.apiVersion = nodeRequest.body.apiVersion);
  return nodeRequest.body.items;
};

const getServiceAccounts = async (identifier, token, namespace) => {
  const client = k8sClient.getCoreApiClient(identifier, token);
  const saRequest = await client.listNamespacedServiceAccount(namespace);
  saRequest.body.items.forEach(i => i.apiVersion = saRequest.body.apiVersion);
  return saRequest.body.items;
};

const getServices = async (identifier, token, namespace) => {
  const client = k8sClient.getCoreApiClient(identifier, token);
  const serviceRequest = await client.listNamespacedService(namespace);
  serviceRequest.body.items.forEach(i => i.apiVersion = serviceRequest.body.apiVersion);
  return serviceRequest.body.items;
};

const getEndpoints = async (identifier, token, namespace) => {
  const client = k8sClient.getCoreApiClient(identifier, token);
  const endpointRequest = await client.listNamespacedEndpoints(namespace);
  endpointRequest.body.items.forEach(i => i.apiVersion = endpointRequest.body.apiVersion);
  return endpointRequest.body.items;
};

const getKustomizations = async (identifier, token, namespace) => {
  const client = k8sClient.getCustomObjectsApiClient(identifier, token);
  const kustomizationResponse = await client.listNamespacedCustomObject("kustomize.toolkit.fluxcd.io", "v1beta2", namespace, "kustomizations");
  kustomizationResponse.body.items.forEach(i => i.apiVersion = kustomizationResponse.body.apiVersion);
  return kustomizationResponse.body.items;
};

const getHelmReleases = async (identifier, token, namespace) => {
  const client = k8sClient.getCustomObjectsApiClient(identifier, token);
  const releases = await client.listNamespacedCustomObject("helm.toolkit.fluxcd.io", "v2beta1", namespace, "helmreleases");
  releases.body.items.forEach(i => i.apiVersion = releases.body.apiVersion);
  return releases.body.items;
};

const getCertificates = async (identifier, token, namespace) => {
  const client = k8sClient.getCustomObjectsApiClient(identifier, token);
  const certRequest = await client.listNamespacedCustomObject("cert-manager.io", "v1", namespace, "certificates");
  certRequest.body.items.forEach(i => i.apiVersion = certRequest.body.apiVersion);
  return certRequest.body.items;
};

const getJobs = async (identifier, token, namespace) => {
  const client = k8sClient.getBatchApiClient(identifier, token);
  const jobRequest = await client.listNamespacedJob(namespace);
  jobRequest.body.items.forEach(i => i.apiVersion = jobRequest.body.apiVersion);
  return jobRequest.body.items;
};

const getCronJobs = async (identifier, token, namespace) => {
  const client = k8sClient.getBatchApiClient(identifier, token);
  const cronJobRequest = await client.listNamespacedCronJob(namespace);
  cronJobRequest.body.items.forEach(i => i.apiVersion = cronJobRequest.body.apiVersion);
  return cronJobRequest.body.items;
};

const getIngresses = async (identifier, token, namespace) => {
  const client = k8sClient.getNetworkingApiClient(identifier, token);
  const ingressRequest = await client.listNamespacedIngress(namespace);
  ingressRequest.body.items.forEach(i => i.apiVersion = ingressRequest.body.apiVersion);
  return ingressRequest.body.items;
};

const getPodMetrics = async (identifier, token, namespace) => {
  const client = k8sClient.getCustomObjectsApiClient(identifier, token);
  const metricsRequest = await client.listNamespacedCustomObject("metrics.k8s.io", "v1beta1", namespace, "pods")
  return metricsRequest.body.items;
};

const getPodLogs = async (identifier, token, namespace, podName, containerName) => {
  const logClient = k8sClient.getLogClient(identifier, token);
  const logStream = new stream.PassThrough();
  logClient.log(namespace, podName, containerName, logStream, { tailLines: 500 });
  const log = await streamToString(logStream);
  return log;
};

function streamToString (stream) {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on("data", (chunk) => chunks.push(Buffer.from(chunk)));
    stream.on("error", (err) => reject(err));
    stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
  })
}

module.exports = {
  getCertificates,
  getCronJobs,
  getDeployments,
  getEndpoints,
  getHelmReleases,
  getIngresses,
  getJobs,
  getKustomizations,
  getNodes,
  getPods,
  getPodLogs,
  getPodMetrics,
  getServices,
  getServiceAccounts,
};
