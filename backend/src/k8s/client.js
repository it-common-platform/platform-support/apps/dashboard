const k8s = require("@kubernetes/client-node");
const NodeCache = require("node-cache");
const config = require("../config");

const cache = new NodeCache();

const CLIENT_MODE = config.getClientMode({
  useClones : false,
});

/**
 * @returns {k8s.CoreV1Api}
 */
 function getCoreApiClient(identifier, authToken = "") {
  if (!cache.has(`coreClient-${identifier}`)) {
    const config = getConfig(identifier, authToken);
    cache.set(`coreClient-${identifier}`, config.makeApiClient(k8s.CoreV1Api));
  }
  return cache.take(`coreClient-${identifier}`);
}

/**
 * @returns {k8s.AppsV1Api}
 */
function getAppsApiClient(identifier, authToken = "") {
  if (!cache.has(`appsClient-${identifier}`)) {
    const config = getConfig(identifier, authToken);
    cache.set(`appsClient-${identifier}`, config.makeApiClient(k8s.AppsV1Api));
  }
  return cache.take(`appsClient-${identifier}`);
}

/**
 * @returns {k8s.BatchV1Api}
 */
function getBatchApiClient(identifier, authToken = "") {
  if (!cache.has(`batchClient-${identifier}`)) {
    const config = getConfig(identifier, authToken);
    cache.set(`batchClient-${identifier}`, config.makeApiClient(k8s.BatchV1Api));
  }
  return cache.take(`batchClient-${identifier}`);
}

/**
 * @returns {k8s.NetworkingV1Api}
 */
function getNetworkingApiClient(identifier, authToken = "") {
  if (!cache.has(`networkingClient-${identifier}`)) {
    const config = getConfig(identifier, authToken);
    cache.set(`networkingClient-${identifier}`, config.makeApiClient(k8s.NetworkingV1Api));
  }
  return cache.take(`networkingClient-${identifier}`);
}

/**
 * @returns {k8s.CustomObjectsApi}
 */
function getCustomObjectsApiClient(identifier, authToken = "") {
  if (!cache.has(`customObjects-${identifier}`)) {
    const config = getConfig(identifier, authToken);
    cache.set(`customObjects-${identifier}`, config.makeApiClient(k8s.CustomObjectsApi));
  }
  return cache.take(`customObjects-${identifier}`);
}

function getConfig(identifier, authToken = "") {
  if (!cache.has(`config-${identifier}`)) {
    cache.set(`config-${identifier}`, makeClientConfig(authToken));
  }
  return cache.take(`config-${identifier}`);
}

function makeClientConfig(authToken = "") {
  const kc = new k8s.KubeConfig();

  if (CLIENT_MODE === "LOCAL_KUBECONFIG") {
    kc.loadFromFile(config.getKubeConfigFile());
  } else if (CLIENT_MODE === "AUTH_HEADER" || CLIENT_MODE === "OAUTH") {
    const options = {
      clusters: [
        { name : "cluster", server: config.getKubeApiEndpoint() }
      ],
      users: [
        { name : "user", user : { token: authToken }}
      ],
      contexts: [
        { name: "context", user: "user", cluster: "cluster" }
      ],
      currentContext: "context"
    };

    kc.loadFromOptions(options);
  } else if (CLIENT_MODE === "SERVICE_ACCOUNT") {
    kc.loadFromCluster();
  }

  return kc;
}

function getWatcher(identifier, authToken) {
  return new k8s.Watch(getConfig(identifier, authToken));
}

function getLogClient(identifier, authToken) {
  return new k8s.Log(getConfig(identifier, authToken));
}

module.exports = {
  getCoreApiClient,
  getAppsApiClient,
  getNetworkingApiClient,
  getBatchApiClient,
  getCustomObjectsApiClient,
  getConfig,
  getWatcher,
  getLogClient,
};
