const config = require("../config");

function configureApp(app) {
  app.get("/api/whoami", (req, res) => {
    res.send({});
  });
}

const getAuthToken = (_) => {
  return null;
};

const configHandler = (req, res) => {
  res.send({
    authRequired: false,
    kubeConfig: {
      clusterName: config.getClusterName(),
    },
    grafanaTemplates: config.getGrafanaTemplates()
  });
};

module.exports = {
  configureApp,
  getAuthToken,
  configHandler,
};