const config = require("../config");

function configureAuth(app) {
  let configHandler = (req, res) => {
    res.send({
      authRequired: false,
    });
  }
  
  app.get("/config", (req, res) => configHandler(req, res));

  const authHandler = getAuthHandler(config.getClientMode());
  authHandler.configureApp(app);
  configHandler = authHandler.configHandler;
}

function getAuthHandler(clientMode) {
  switch (clientMode) {
    case "OAUTH":
      return require("./oauth");
    case "AUTH_HEADER":
      return require("./authHeader");
    case "LOCAL_KUBECONFIG":
      return require("./kubeconfig");
    default:
      throw new Error(`Unrecognized client mode: ${clientMode}`);
  }
}

module.exports = {
  configureAuth,
};