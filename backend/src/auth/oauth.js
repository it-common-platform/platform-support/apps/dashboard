const passport = require("passport");
const OAuth2Strategy = require("passport-oauth").OAuth2Strategy;
const jwt = require("jsonwebtoken");
const config = require("../config");

function configureApp(app) {
  passport.use("provider", new OAuth2Strategy({
    authorizationURL: config.getOauthAuthorizationUrl(),
    tokenURL: config.getOauthTokenUrl(),
    clientID: config.getOauthClientId(),
    clientSecret: config.getOauthClientSecret(),
    callbackURL: `${config.getBaseUrl()}/auth/callback`,
    scope: "openid email profile groups",
  }, (accessToken, refreshToken, params, profile, done) => {
    const user = jwt.decode(params.id_token);
    done(null, { user, accessToken })
  }));

  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, JSON.stringify(user));
  });

  passport.deserializeUser((user, done) => {
    done(null, JSON.parse(user));
  });

  app.get("/auth", passport.authenticate("provider", { session: true }));
  app.get("/auth/callback", passport.authenticate("provider", { successReturnToOrRedirect: "/", session: true, }));
  app.get("/auth/logout", (req, res) => { req.logout(); res.send({ success: true })});
  app.use((req, res, next) => {
    if (req.path.startsWith("/api") && !req.isAuthenticated())
      return res.send(401);

    return next();
  });

  app.get("/api/whoami", (req, res) => {
    res.send({
      accessToken: req.user.accessToken,
      user: req.user.user,
    });
  });
}

const getAuthToken = (req) => {
  return req.user.accessToken;
};

const configHandler = (req, res) => {
  res.send({
    authRequired: true,
    redirectUri: "/auth",
    kubeConfig: {
      apiEndpoint: config.getKubeApiEndpoint(),
      clusterName: config.getClusterName(),
    },
    grafanaTemplates: config.getGrafanaTemplates()
  });
};

module.exports = {
  configureApp,
  getAuthToken,
  configHandler,
};
