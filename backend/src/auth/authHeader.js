const jwt = require("express-jwt");
const jwksRsa = require("jwks-rsa");
const config = require("../config");

function configureApp(app) {
  app.use(jwt({
    secret: jwksRsa.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: config.getJwksEndpoint(),
    }),

    audience: config.getJwtAudience(),
    issuer: config.getJwtIssuer(),
    algorithms: [ 'RS256' ],
  }));
}

const getAuthToken = (req) => {
  return req.headers.authorization.split(" ")[1];
};

const configHandler = (req, res) => {
  res.send({
    authRequired: true,
  });
};

module.exports = {
  configureApp,
  getAuthToken,
  configHandler,
};