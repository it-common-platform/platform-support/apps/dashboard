const fs = require("fs");

const fileConfig = require('/usr/local/app/config.json');

function getClientMode() {
  // Supports LOCAL_KUBECONFIG, OAUTH
  return process.env.CLIENT_MODE;
}

function getKubeConfigFile() {
  return process.env.KUBE_CONFIG_FILE_LOCATION
    ? process.env.KUBE_CONFIG_FILE_LOCATION
    : "/root/.kube/config";
}

function getKubeApiEndpoint() {
  return process.env.K8S_API_ENDPOINT;
}

function getJwksEndpoint() {
  return process.env.JWKS_ENDPOINT;
}

function getJwtAudience() {
  return process.env.JWT_AUDIENCE;
}

function getJwtIssuer() {
  return process.env.JWT_ISSUER;
}

function getOauthClientId() {
  return process.env.OAUTH_CLIENT_ID;
}

function getOauthClientSecret() {
  if (!process.env.OAUTH_CLIENT_SECRET_FILE) {
    throw new Error("No OAUTH_CLIENT_SECRET_FILE defined");
  }

  return fs.readFileSync(process.env.OAUTH_CLIENT_SECRET_FILE).toString();
}

function getTokenAppUrl() {
  return process.env.TOKEN_APP_URL;
}

function getBaseUrl() {
  return process.env.BASE_URL;
}

function getRedisHost() {
  return process.env.REDIS_HOST ? process.env.REDIS_HOST : "redis";
}

function getOauthAuthorizationUrl() {
  return process.env.OAUTH_AUTHORIZATION_URL;
}

function getOauthTokenUrl() {
  return process.env.OAUTH_TOKEN_URL;
}

function getHostName() {
  return process.env.HOST_NAME;
}

function getSealedSecretCertLocation() {
  return process.env.SEALED_SECRET_CERT_URL;
}

function getClusterName() {
  return process.env.CLUSTER_NAME
    ? process.env.CLUSTER_NAME
    : "platform-cluster";
}

function getGrafanaTemplates() {
  if (fileConfig.grafanaTemplates) {
    return fileConfig.grafanaTemplates;
  } else {
    return {};
  }
}

module.exports = {
  getClientMode,
  getKubeConfigFile,
  getKubeApiEndpoint,
  getJwksEndpoint,
  getJwtAudience,
  getJwtIssuer,
  getOauthClientId,
  getOauthClientSecret,
  getTokenAppUrl,
  getBaseUrl,
  getRedisHost,
  getOauthAuthorizationUrl,
  getOauthTokenUrl,
  getHostName,
  getSealedSecretCertLocation,
  getClusterName,
  getGrafanaTemplates,
};
