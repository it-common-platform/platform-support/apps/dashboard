FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/node:lts-alpine AS base

FROM base AS clientBuild
WORKDIR /usr/local/app
COPY client/package.json client/yarn.lock client/config-overrides.js ./
RUN yarn install
COPY client/public ./public
COPY client/src ./src
RUN yarn test --watchAll=false && yarn build

FROM base
WORKDIR /usr/local/app
COPY backend/package.json backend/yarn.lock ./
RUN yarn install --production
COPY backend/src ./src
COPY --from=clientBuild /usr/local/app/build /usr/local/app/src/client
COPY backend/config.json ./
ARG VERSION
ENV VERSION=${VERSION}
RUN test -n "${VERSION}" && \
    echo "{\"version\":\"$VERSION\"}" > /usr/local/app/src/client/version.json
CMD ["node", "src/index.js"]
